<?php
include('includes/conn.php');

	if(isset($_SESSION['admin_id']))
	{
		unset($_SESSION['admin_id']);
		unset($_SESSION['admin_name']);
		unset($_SESSION['admin_email']);
		session_destroy();
	}
	header('Location: login.php');
	exit();
?>