<!DOCTYPE html>
<html>
<head>

    <title>Beachbuggy</title>

    <!-- Core CSS - Include with every page -->
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <script src="js/validation.js" type="text/javascript"></script>
    <link href="http://ssharunas.github.io/bootstrap-timepicker/assets/bootstrap-timepicker/css/timepicker.css" type="text/css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
		span#error,#errorstar{ color:#F00; font-weight:bold;}
	</style>
	
	
  

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Beachbuggy</a>
				
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
				<li>
					<span ><b>Welcome</b> <?php echo $_SESSION['admin_name']; ?></span>
				</li>
			
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
						
						<li>
                            <a href="pickup.php"><i class="fa fa-user fa-fw"></i> Pickups </a>
                        </li>
						
						<li>
                            <a href="driver.php"><i class="fa fa-user fa-fw"></i> Drivers </a>
                        </li>
						
						<li>
                            <a href="rider.php"><i class="fa fa-user fa-fw"></i> Riders </a>
                        </li>
						
						<li>
                            <a href="places.php"><i class="fa fa-user fa-fw"></i> Popular Places </a>
                        </li>
						
						<li>
                            <a href="setting.php"><i class="fa fa-user fa-fw"></i>Setting</a>
                        </li>
                       
					    <li>
                            <a href="advertise.php"><i class="fa fa-user fa-fw"></i>Advertise</a>
                        </li>
					   
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>