<?php
include('includes/conn.php');
 include('includes/header.php');
		$id = $_GET['id'];
$sql="select * from tblPopularPlaces WHERE `pkPlaceId` = $id ";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);

//print_r($row);
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}

 ?>
 <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Popular Places</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Driver Information
							<a  href="driver.php" class="btn btn-primary btn-xs" style="float:right" >Back</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" action="submit_places.php">
										<input type="hidden" name="edit" value="edit"/>
										<input type="hidden" name="id" value="<?php echo $row['pkPlaceId'];?>"/>
                                        <span id="error"> * is Required Field </span>
                                        <div class="form-group">
                                            <label>Name</label>  <span id="errorstar">*</span>
                                            <input class="form-control" placeholder="Enter Name" name="name" required value="<?php echo $row['name'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Latitude</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="lat" placeholder="Enter Latitude" required value="<?php echo $row['latitude'];?>">
                                        </div>
										<div class="form-group">
                                            <label>Longitude</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="long" placeholder="Enter longitude" required value="<?php echo $row['longitude'];?>">
                                        </div>
										
										<div class="form-group">
                                            <label> Status : <label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" value="1" <?php echo ($row['status']== 1) ?  "checked" : ""; ?>>Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" value="0" <?php echo ($row['status']== 0) ?  "checked" : ""; ?>>No
                                            </label>
                                        </div>

                                        <button type="submit" class="btn btn-success" >Submit Button</button>
                                        <button type="reset" class="btn btn-warning">Reset Button</button>
                                    </form>
										
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Forms -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Forms - Use for reference -->

</body>

</html>
