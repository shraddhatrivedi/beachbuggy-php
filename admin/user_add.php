<?php 
include('includes/conn.php');
include('includes/header.php');
$sql="select * from user";
$result = mysqli_query($con,$sql);
//$row = mysqli_fetch_array($result);

//print_r($row);

if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
 ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Adding User Information
							<a  href="user.php" class="btn btn-primary btn-xs" style="float:right" >Back</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" name="add_user" method="post" action="user_submit.php"  enctype="multipart/form-data" onsubmit="return user_validation();">
										<input type="hidden" name="add" id="add" value="add"/>
                                        <span id="error"> * is Required Field </span>
                                        <div class="form-group">
                                            <label>First Name</label>  <span id="errorstar">*</span>
                                            <input class="form-control" placeholder="Enter Your First Name" name="fname" id="fname">
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="lname" id="lname" placeholder="Enter Your First Name">
                                        </div>
										<div class="form-group">
                                            <label>User Name</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="username" id="username" placeholder="Enter Your Username">
                                        </div>
										<div class="form-group">
                                            <label>Email </label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="email" id="email" type="email" placeholder="Enter Your Email">
                                        </div>
										<div class="form-group">
                                            <label>Password </label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="password" id="password" type="password" placeholder="Enter Your Password">
                                        </div>
										<div class="form-group">
                                            <label>Phone No </label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="ph_no" id="ph_no" placeholder="Enter Your Phone number" maxlength="11">
                                        </div>
										<div class="form-group">
                                            <label>Address </label>
                                            <textarea class="form-control ckeditor" rows="3" name="address" id="address"></textarea>
                                        </div>
										<div class="form-group">
                                            <label> Gender : <label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" id="optionsRadiosInline1" value="male" checked="checked">Male
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" id="optionsRadiosInline2" value="female">Female
                                            </label>
                                             
                                        </div>

										<div class="form-group">
                                            <label>Country</label>  <span id="errorstar">*</span>
                                            <?php
													$sql_country = "SELECT * FROM `country` WHERE `c_id`='0'";
													$res_country = mysqli_query($con,$sql_country);
												 ?>
                                            	
                                            <select class="form-control" id="country" name="country">
                                            <option id="0" value="0">Please Select Country</option>
                                            <?php while($country = mysqli_fetch_array($res_country)){?>
                                            	<option id="<?php echo $country['id']; ?>" value="<?php echo $country['name']; ?>" ><?php echo $country['name']; ?></option>
                                             <?php } ?>
                                            </select> 
                                        </div>
										<div class="form-group">
                                            <label>State </label>  <span id="errorstar">*</span>
                                              <?php
													$sql_state = "SELECT * FROM `country` WHERE `c_id`!='0'";
													$res_state = mysqli_query($con,$sql_state);
											 ?>

                                            <select class="form-control" id="state" name="state">
                                            <option id="0" value="0">Please Select State</option>
                                            <?php while($state = mysqli_fetch_array($res_state)){?>
                                            	<option id="<?php echo $state['id']; ?>" value="<?php echo $state['name']; ?>" ><?php echo $state['name']; ?></option>
                                             <?php } ?>
                                            </select> 
                                        </div>
										<div class="form-group">
                                            <label>City </label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="city" id="city" placeholder="Enter Your City"> 
                                        </div>
										<div class="form-group">
                                            <label>User Type</label> <span id="errorstar">*</span>   
                                            <?php
													$sql_usertype = "SELECT * FROM `user_type`";
													$res_usertype = mysqli_query($con,$sql_usertype);
											 ?>

                                            <select class="form-control" id="user_type" name="user_type">
                                            <option id="0" value="0">Please Select User Type</option>
                                            <?php while($usertype = mysqli_fetch_array($res_usertype)){?>
                                            	<option id="<?php echo $usertype['id']; ?>" value="<?php echo $usertype['title']; ?>" ><?php echo $usertype['title']; ?></option>
                                             <?php } ?>
                                            </select>                                     
                                        </div>
										<div class="form-group">
                                            <label>Upload Image</label>
                                            <input type="file" name="file" id="file">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-success" >Submit Button</button>
                                        <button type="reset" class="btn btn-warning">Reset Button</button>
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Forms -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Forms - Use for reference -->

</body>

</html>
