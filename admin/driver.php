<?php
include('includes/conn.php');
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
include('includes/header.php');
$sql="select * from tbldriver";
$result = mysqli_query($con,$sql);
//$row = mysqli_fetch_array($result);

//print_r($row);
if($_GET['reset'])
{
	$object = $_GET['object'];
	//include('object_change.php');
	
	$url = 'https://api.parse.com/1/classes/_Installation/'.$object;
error_reporting(E_ERROR);
    //open connection
    $ch = curl_init();
//$qry_str = "X-Parse-Application-Id='WEV1ytxUVnWX8JM2j3LXsJ6n7m4cyPFPOACuUpr7'&X-Parse-REST-API-Key='91LwLHm46CfILBnvgE7ZUUgh3Mvu2LIflR5v21tM'";
$MyApplicationId = "WEV1ytxUVnWX8JM2j3LXsJ6n7m4cyPFPOACuUpr7";
$MyParseRestAPIKey = "91LwLHm46CfILBnvgE7ZUUgh3Mvu2LIflR5v21tM";

$headers = array(
    "Content-Type: application/json",
    "X-Parse-Application-Id: " . $MyApplicationId,
    "X-Parse-REST-API-Key: " . $MyParseRestAPIKey
);

$data = array('emailID'=>'');
$data_json = json_encode($data);
    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    //curl_setopt($ch,CURLOPT_POST,1);
	//curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
    curl_setopt($ch, CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $qry_str);

    //execute post
    $result = curl_exec($ch);

    //close connection
    curl_close($ch);
	//json_encode($result);
	
	
	
	$sql= "Update `tbldriver` SET `accessToken` = '' where pkDriverId =".$_GET['reset'];
	if (mysqli_query($con,$sql))
	{
		header('Location: driver.php');
		//die('Error: ' . mysqli_error($con));
	}
}

 ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Driver</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Change All Driver Queue Size
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								    <div class="extra_title" style="color:red;">Current General Queue size is 3</div><br/>
                                    <form role="form" name="edit_user" method="post" action="submit_driver.php" enctype="multipart/form-data" onsubmit="return user_validation();">
										    <div class="form-group">
											<input type="hidden" name="change" id="change" value="change"/>
                                            <label>Change Queue Size</label> <span id="errorstar">*</span>   
                                            <input class="form-control" placeholder="Enter Queue Size" name="queuesize" required>
											</div>
											<button type="submit" class="btn btn-success">Submit</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Driver Data Table
							<!--<a  href="page_add.php" class="btn btn-primary btn-xs" style="float:right" >Add Pages</a>-->
                        </div>
						
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
											<th>Email ID</th>
											<th>Address</th>
											<th>Phone No</th>
											<th>Licence Number</th>
											<th>Queue Size</th>
											<th>Status</th>
                                           <th style="text-align:center">Action</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
									
									<?php
									//print_r($result);
									while($row = mysqli_fetch_array($result)){?>
                                        <tr class="odd gradeX">
                                            
                                            <td><?php echo $row['firstName'].' '.$row['lastName']; ?></td>
											<td><?php echo $row['emailID'] ; ?></td>
											<td><?php echo $row['homeAddress'] ; ?></td>
											<td><?php echo $row['phoneNo'] ; ?></td>
											<td><?php echo $row['licenseNumber'] ; ?></td>
											<td><?php echo $row['queueSize'] ; ?></td>
											<td><?php echo $row['status'] ; ?></td>
											<td align="center">
												<a href="edit_driver.php?driverid=<?php echo $row['pkDriverId'];   ?>" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i></a>
												<?php if($row['accessToken'] != '')
												{
												?>
												<a class="btn btn-primary btn-xs" href="driver.php?reset=<?php echo $row['pkDriverId']; ?>&object=<?php echo $row['objectID'];?>" onclick="return confirm('Are You Sure?');">Reset Driver</a>
												<?php
												}
												?>
											</td>
                                            
                                        </tr>
										<?php } ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
