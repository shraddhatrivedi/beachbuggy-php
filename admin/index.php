<?php
include('includes/conn.php');
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}


?>
 <?php
 include('includes/header.php');
 $p_sql = "select * from `tblPickUp`";
 $p_res = mysqli_query($con,$p_sql);
 $p_count = mysqli_num_rows($p_res);
 
 $d_sql = "select * from `tbldriver`";
 $d_res = mysqli_query($con,$d_sql);
 $d_count = mysqli_num_rows($d_res);
 
 $r_sql = "select * from `tblUser`";
 $r_res = mysqli_query($con,$r_sql);
 $r_count = mysqli_num_rows($r_res);
?>
  <style>
  .ct {
  text-align: center;
}
.col-lg-12.ct {
  color: #428bca;
  font-size: 30px;
}
  </style>  

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
              <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                        <div class="row">
                                <div class="col-lg-4">
									 <div class="panel panel-default">
                        <div class="panel-heading ct">Total Pickups</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 ct">
                                    <?php echo $p_count; ?>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->  
                                </div>
								<div class="col-lg-4">
									 <div class="panel panel-default">
                        <div class="panel-heading ct">Total Drivers</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 ct">
                                    <?php echo $d_count; ?>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                                </div>
								<div class="col-lg-4">
									<div class="panel panel-default">
                        <div class="panel-heading ct">Total Riders</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 ct">
                                    <?php echo $r_count; ?>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                                </div>
                               
                        </div>
                            <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="js/demo/dashboard-demo.js"></script>

</body>

</html>
