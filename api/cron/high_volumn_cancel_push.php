<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
$data = json_encode(array("action_key"=>106,"alert"=>"Your ride was cancelled by server. Please request a ride again when you are ready","content-available"=>1,"sound" => "default"));
//add for ios requirement as samirsir suggestion "content-available": 1 and "sound" : "default"

// Push to Channels
/*ParsePush::send(array(
  "channels" => ["driver"],
  "data" => $data
));*/
$userid = (int)$userid;
// Push to Query
$query = ParseInstallation::query();
//$query->equalTo('deviceType', 'android');
//$query->equalTo('email',$emailId);
$query->equalTo('userID',$userid);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));


?>
