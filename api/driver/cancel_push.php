<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
$data = json_encode(array("action_key"=>104,"","pickupid"=>$pickupid,"alert" => 'Your ride has been cancelled by driver('.$driver_name.').Because '.$message.'.Please claim for a new ride.',"content-available"=>1,"sound" => "default"));
//add for ios requirement as samirsir suggestion "content-available": 1

// Push to Channels
/*ParsePush::send(array(
  "channels" => ["driver"],
  "data" => $data
));*/
//echo "====".$userid."====";
// Push to Query

$userid = (int)$userid;
$query = ParseInstallation::query();
//$query->equalTo('deviceType', 'android');
$query->equalTo('userID',$userid);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));
//var_dump($query);
?>
