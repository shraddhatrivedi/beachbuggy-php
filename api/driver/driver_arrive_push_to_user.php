<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
date_default_timezone_set("US/Eastern");
$cur_time = strtotime('now'); 
$cur_long = $cur_time * 1000;
$data = json_encode(array("action_key"=>102,"timestamp"=>$cur_long,"alert"=>"Your Beachside Buggy is here! You have 3 minutes to claim your free ride!","content-available"=>1,"sound" => "driver_arrived.caf"));
//add for ios requirement as samirsir suggestion "content-available": 1


// Push to Channels
/*ParsePush::send(array(
  "channels" => ["driver"],
  "data" => $data
));*/
$userid = (int)$userid;
// Push to Query
$query = ParseInstallation::query();
$query->equalTo('deviceType', 'android');
$query->equalTo('userID',$userid);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));


$query_ios = ParseInstallation::query();
$query_ios->equalTo('deviceType', 'ios');
$query_ios->equalTo('userID',$userid);
ParsePush::send(array(
  "where" => $query_ios,
  "data" => $data
));


?>
