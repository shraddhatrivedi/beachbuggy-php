<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
$data = json_encode(array("action_key"=>5,"pickupid"=>$pickupid,"alert" => 'You win the bid to Pickup '.$user_name.' from '.$destination_address,"sound" => "default"));
//echo $driveremailId;

// Push to Query
$query = ParseInstallation::query();
//$query->equalTo('deviceType', 'android');
$query->equalTo('emailID',$driveremailId);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));


$user_data = json_encode(array("action_key"=>101,"alert"=>"Your driver ".$driver_name." will be there in approximately ".$pickup_time." minutes. You will be notified when your Beachside Buggie arrives!","content-available"=>1,"sound" => "default"));
//add for ios requirement as samirsir suggestion "content-available": 1
$userid = (int)$userid;
//echo $userid;
$user_query = ParseInstallation::query();
//$user_query->equalTo('deviceType', 'android');
//$user_query->equalTo('emailID','beachBuggyRider');
$user_query->equalTo('userID',$userid);
ParsePush::send(array(
  "where" => $user_query,
  "data" => $user_data
));

//var_dump($user_query);
?>
